################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ufft/fft-dif.c \
../ufft/fft-dit.c \
../ufft/ift-dif.c \
../ufft/ift-dit.c 

C_DEPS += \
./ufft/fft-dif.d \
./ufft/fft-dit.d \
./ufft/ift-dif.d \
./ufft/ift-dit.d 

OBJS += \
./ufft/fft-dif.obj \
./ufft/fft-dit.obj \
./ufft/ift-dif.obj \
./ufft/ift-dit.obj 

OBJS__QUOTED += \
"ufft\fft-dif.obj" \
"ufft\fft-dit.obj" \
"ufft\ift-dif.obj" \
"ufft\ift-dit.obj" 

C_DEPS__QUOTED += \
"ufft\fft-dif.d" \
"ufft\fft-dit.d" \
"ufft\ift-dif.d" \
"ufft\ift-dit.d" 

C_SRCS__QUOTED += \
"../ufft/fft-dif.c" \
"../ufft/fft-dit.c" \
"../ufft/ift-dif.c" \
"../ufft/ift-dit.c" 


