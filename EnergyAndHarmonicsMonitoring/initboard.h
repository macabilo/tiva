/* Initializes Tiva C Series Launchpad board.
		Written by:
		Date:
*/
#ifndef INITBOARD_H_
#define INITBOARD_H_

#include "datastruct.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/adc.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"

#define SAMPLING_FREQ       3840ul//7680ul//15360ul
#define VOLTAGE_CHANNEL     ADC_CTL_CH5
#define CURRENT_CHANNEL     ADC_CTL_CH4


void TimerIntHandler (void);
void InitBoard (void);
void _enableSampling (void);
void _disableSampling (void);


#endif // INITBOARD_H_
