#ifndef FFTCOMPUTE_H_
#define FFTCOMPUTE_H_

#include <stdint.h>
#include <stdlib.h>
#define __BSD_VISIBLE // For selecting math functions from math library
#include <math.h>





//void dft (uint16_t N, float* xreal,
//				float* ximag, float* freal,
//				float* fimag);

//void dft (uint16_t N, float* xreal, float* freal);

//void fft(float* xreal, float* ximag, float* freal, float* fimag);
void fft(float* InputR, float* InputI, int N);
#endif
