/* Initialises Tiva C Series Launchpad board.
		Written by:
		Date:
*/
#include "initboard.h"

/* ====================== TimerIntHandler ================================
     Triggers whenever a sampling timer has ticked

         Post       a new sample has been retrieved for the DT signal
 */
void TimerIntHandler (void){
    // Clear ADC mask flag
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    // Signal system flag that a new sample is ready
    isSampleReady = true;
    return;
}

/* ================= enableSampling =====================
    Enable sampling timer to start sampling
        Pre     system configured for sampling
        Post    run sampling timer
*/
void _enableSampling(){
    // At the beginning, acknowledge that the program
    // will wait for a new sample to store
    isSampleReady = false;

    // Run the sampling timer
    TimerEnable(TIMER0_BASE, TIMER_A);
    return;
}

/* ================= disableSampling =================
    Disable sampling timer to stop signal sampling

        Pre     sampling has filled sample array
        Post    sampling timer stopped
 */
void _disableSampling(){
    // Disable the sampling timer
    TimerDisable(TIMER0_BASE, TIMER_A);
    return;
}

/*
 * Run when system initialises to prevent fluctuation at ADC readings
 * due to startup
 *
 *      Pre     initial configuration of the board has been set
 *      Post    ADC has been primed for running
 */
void _stabilizeAdc()
{
// Local Declaration
    uint32_t rawVoltageBuffer[4], rawCurrentBuffer[4];
    uint16_t index;

// Statements
    index = 0;
    _enableSampling();

    while(1){
        if (isSampleReady){
            // Clear ADC interrupt to accommodate new sample for the voltage waveform
            ADCIntClear(ADC0_BASE, SAMPLE_SEQUENCER1);

            // Move data from ADC FIFO to a buffer variable for voltage
            ADCSequenceDataGet(ADC0_BASE, SAMPLE_SEQUENCER1, rawVoltageBuffer);

            //Do the same thing for the current waveform
            ADCIntClear(ADC1_BASE, SAMPLE_SEQUENCER1);
            ADCSequenceDataGet(ADC1_BASE, SAMPLE_SEQUENCER1, rawCurrentBuffer);

            // Set flag to wait for a new sample
            isSampleReady = false;

            // Increment sample index
            index += 1;
            if (index == SAMPLESIZE){
                break;
            }
        }
    }
    _disableSampling();
    return;
}

/* ========================== initBoard ==================================
     Initialises the peripherals of the Tiva board to be used in the
     application.

         Pre        Board boots up
         Post       Microcontroller configured
*/
void InitBoard(void){
	
	// Configure system clock (40MHz)
    SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL |
                SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

	// Enable peripheral clocks
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
//    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
//    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

	// Configure timer peripheral
    TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
    TimerLoadSet(TIMER0_BASE, TIMER_A, SysCtlClockGet()/SAMPLING_FREQ);
    TimerControlTrigger(TIMER0_BASE, TIMER_A, true);

	// Configure ADC peripheral
    ADCSequenceConfigure(ADC0_BASE, SAMPLE_SEQUENCER1,
                        ADC_TRIGGER_TIMER, TOP_PRIORITY);
    ADCSequenceStepConfigure(ADC0_BASE, SAMPLE_SEQUENCER1, 0,
                            VOLTAGE_CHANNEL);
    ADCSequenceStepConfigure(ADC0_BASE, SAMPLE_SEQUENCER1, 1,
                            VOLTAGE_CHANNEL);
    ADCSequenceStepConfigure(ADC0_BASE, SAMPLE_SEQUENCER1, 2,
                            VOLTAGE_CHANNEL);
    ADCSequenceStepConfigure(ADC0_BASE, SAMPLE_SEQUENCER1, 3,
                        VOLTAGE_CHANNEL | ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceEnable(ADC0_BASE, SAMPLE_SEQUENCER1);

    ADCSequenceConfigure(ADC1_BASE, SAMPLE_SEQUENCER1,
                        ADC_TRIGGER_TIMER, TOP_PRIORITY);
    ADCSequenceStepConfigure(ADC1_BASE, SAMPLE_SEQUENCER1, 0,
                        CURRENT_CHANNEL);
    ADCSequenceStepConfigure(ADC1_BASE, SAMPLE_SEQUENCER1, 1,
                        CURRENT_CHANNEL);
    ADCSequenceStepConfigure(ADC1_BASE, SAMPLE_SEQUENCER1, 2,
                        CURRENT_CHANNEL);
    ADCSequenceStepConfigure(ADC1_BASE, SAMPLE_SEQUENCER1, 3,
                        CURRENT_CHANNEL | ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceEnable(ADC1_BASE, SAMPLE_SEQUENCER1);

    // Configure UART
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 115200,
                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE
                                | UART_CONFIG_PAR_NONE));
//    GPIOPinConfigure(GPIO_PB0_U1RX);
//    GPIOPinConfigure(GPIO_PB1_U1TX);
//    GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);
//
//    UARTConfigSetExpClk(UART1_BASE, SysCtlClockGet(), 115200,
//                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE
//                                | UART_CONFIG_PAR_NONE));

	// Configure interrupts
    ADCIntClear(ADC0_BASE, SAMPLE_SEQUENCER1);
    ADCIntClear(ADC1_BASE, SAMPLE_SEQUENCER1);

    ADCIntEnable(ADC0_BASE, SAMPLE_SEQUENCER1);
    ADCIntEnable(ADC1_BASE, SAMPLE_SEQUENCER1);

    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    IntEnable(INT_TIMER0A_TM4C123);

	// Stabilize ADC

	return;
}
