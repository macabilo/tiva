#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "driverlib/adc.h"
#include "driverlib/udma.h"
#include "driverlib/uart.h"

/**
 * main.c
 */
int main(void)
{
#pragma DATA_ALIGN(pui8DMAControlTable, 1024)
    uint8_t pui8DMAControlTable[1024];
    uint8_t pui8SourceBuffer[256];
    uint8_t pui8DestBuffer[256];

    uDMAEnable();
    uDMAControlBaseSet(&pui8DMAControlTable[0]);
    uDMAChannelControlSet(UDMA_CHANNEL_ADC0 | UDMA_PRI_SELECT, UDMA_SIZE_32 | UDMA_SRC_INC_NONE | UDMA_DST_INC_32 | UDMA_ARB_1024);
    uDMAChannelControlSet(UDMA_CHANNEL_ADC0 | UDMA_ALT_SELECT, UDMA_SIZE_32 | UDMA_SRC_INC_NONE | UDMA_DST_INC_32 | UDMA_ARB_1024);
    uDMAChannelTransferSet(UDMA_CHANNEL_ADC0 | UDMA_PRI_SELECT, UDMA_MODE_PINGPONG, (void*)(), pui8DestBuffer, sizeof(pui8DestBuffer));

    uDMAChannelEnable(UDMA_CHANNEL_SW);
    uDMAChannelRequest(UDMA_CHANNEL_SW);

	return 0;
}
