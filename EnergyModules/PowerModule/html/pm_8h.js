var pm_8h =
[
    [ "__BSD_VISIBLE", "group__pm.html#gaa00daaab11d6072cf978ba8268041ed6", null ],
    [ "CURRENT_CHANNEL", "group__pm.html#ga501e37e951966e98511c9c1eb0c7d397", null ],
    [ "INITBOARD", "group__pm.html#gad0d6b1ad78bd661bd8b315dcf0a2f93e", null ],
    [ "SAMPLE_SEQUENCER_1", "group__pm.html#ga72b6e2cd5a6951427db3f69475dcebff", null ],
    [ "SAMPLESIZE", "group__pm.html#gaf675e47c038bc2a7f746eba51da99fe7", null ],
    [ "SAMPLING_RATE", "group__pm.html#ga17889cf1cf83a54524c242fa5a353cf1", null ],
    [ "VOLTAGE_CHANNEL", "group__pm.html#ga3ad11a109af35fa8de5e0b1b3e60142b", null ],
    [ "compute_fd", "group__pm.html#ga85de642b038fffb57d06d642e5defebd", null ],
    [ "compute_td", "group__pm.html#ga24de6e69923681e21471d5aa5fab1362", null ],
    [ "init_board", "group__pm.html#gad44ec0e3bb45edf653bd04c37508e269", null ],
    [ "sample_signal", "group__pm.html#gaca43e3e6a73564ac9902515e510d453d", null ],
    [ "vectorize_signal", "group__pm.html#ga26efba4814a31d11753389cfd6c45787", null ]
];