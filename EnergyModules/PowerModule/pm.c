
#include "pm.h"

void board_init(uint32_t fs, uint32_t vc, uint32_t ic, rawData_ptr raw){
    ROM_FPULazyStackingEnable();
    ROM_FPUEnable();

    ROM_SysCtlClockSet(SYSCTL_SYSDIV_10 | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN | SYSCTL_USE_PLL); //20MHz

    sampling_clk_init(fs);
    adc_init(ADC0_BASE, vc);
    adc_init(ADC1_BASE, ic);
    dma_init(UDMA_CHANNEL_ADC0, (void*)(ADC0_BASE + ADC_O_SSFIFO1), &raw->voltage[0]);
    dma_init(UDMA_CHANNEL_ADC1, (void*)(ADC1_BASE + ADC_O_SSFIFO1), &raw->current[0]);
}

void adc_init(uint32_t adc_base, uint32_t channel){
    if(adc_base == ADC0_BASE)
        ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    else
        ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);
    ROM_SysCtlDelay(10); //delays for 10*3/2000000 s

    ROM_ADCSequenceConfigure(adc_base, 1, ADC_TRIGGER_TIMER, 0);
    ROM_ADCSequenceStepConfigure(adc_base, 1, 0, channel);
    ROM_ADCSequenceStepConfigure(adc_base, 1, 1, channel);
    ROM_ADCSequenceStepConfigure(adc_base, 1, 2, channel);
    ROM_ADCSequenceStepConfigure(adc_base, 1, 3, channel | ADC_CTL_IE | ADC_CTL_END);
    ROM_ADCSequenceEnable(adc_base, 1);
}

void dma_init(uint32_t channel, void* src, void* dest){

    uint8_t dmactlTable[1024];

    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);
    ROM_SysCtlDelay(10);

    ROM_uDMAEnable();
    ROM_uDMAControlBaseSet(&dmactlTable[0]);
    ROM_uDMAChannelControlSet(channel | UDMA_PRI_SELECT, \
                              UDMA_SIZE_32 | UDMA_SRC_INC_NONE | UDMA_DST_INC_32 | UDMA_ARB_1024);
    ROM_uDMAChannelControlSet(channel | UDMA_ALT_SELECT, \
                              UDMA_SIZE_32 | UDMA_SRC_INC_NONE | UDMA_DST_INC_32 | UDMA_ARB_1024);
    ROM_uDMAChannelTransferSet(channel | UDMA_PRI_SELECT, \
                               UDMA_MODE_PINGPONG, src, dest, sizeof(uint32_t)*1024);
    ROM_uDMAChannelTransferSet(channel | UDMA_ALT_SELECT, \
                               UDMA_MODE_PINGPONG, src, dest, sizeof(uint32_t)*1024);

}

void sampling_clk_init(uint32_t period){
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    ROM_SysCtlDelay(10);

    ROM_TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
    ROM_TimerLoadSet(TIMER0_BASE, TIMER_A, CLOCK_SECOND/period);
    TimerADCEventSet(TIMER0_BASE, TIMER_ADC_TIMEOUT_A);
}

void sample_signal(rawData_ptr raw){
    //request dma???
}

void reconstruct_signal();
void locate_zero_cross();
void td_compute();
