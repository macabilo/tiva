#ifndef PM_H
#define PM_H

#define __BSD_VISIBLE
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#define TARGET_IS_TM4C123_RA1
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_adc.h"
#include "driverlib/adc.h"
#include "driverlib/fpu.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/udma.h"

#define CLOCK_SECOND \
    ROM_SysCtlClockGet()

typedef struct tdData{
    float vrms;
    float irms;
    float freq;
    float ppwr;
    float qpwr;
    float spwr;
    float pf;
}dataTD_t, *dataTD_ptr;

typedef struct sigData{
    float voltage[256];
    float current[256];
}sigData_t, *sigData_ptr;

typedef struct rawData{
    int32_t voltage[1024];
    int32_t current[1024];
}rawData_t, *rawData_ptr;

extern void board_init(uint32_t fs, uint32_t vc, uint32_t ic, rawData_ptr raw);
extern void adc_init(uint32_t adc_base, uint32_t channel);
extern void dma_init(uint32_t channel, void* src, void* dest);
extern void sampling_clk_init(uint32_t period);
extern void sample_signal();
extern void reconstruct_signal();
extern void locate_zero_cross();
extern void td_compute();
extern void sampling_clk_enable();
extern void sampling_clk_disable();

#endif
