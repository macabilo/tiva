################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
S_UPPER_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM0.S \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM3.S \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM4.S 

LD_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/ARMCMx.ld 

C_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM0.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM3.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM4.c 

C_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM0.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM3.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM4.d 

OBJS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM0.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM3.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM4.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM0.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM3.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM4.obj 

S_UPPER_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM0.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM3.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM4.d 

S_UPPER_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\startup_ARMCM0.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\startup_ARMCM3.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\startup_ARMCM4.d" 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\startup_ARMCM0.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\startup_ARMCM3.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\startup_ARMCM4.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\system_ARMCM0.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\system_ARMCM3.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\system_ARMCM4.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\system_ARMCM0.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\system_ARMCM3.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Examples\arm_dotproduct_example\GCC\Startup\system_ARMCM4.d" 

S_UPPER_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM0.S" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM3.S" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/startup_ARMCM4.S" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM0.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM3.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Examples/arm_dotproduct_example/GCC/Startup/system_ARMCM4.c" 


