################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
S_UPPER_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal2.S 

C_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix8_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_init_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q31.c 

C_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix8_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_init_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q31.d 

OBJS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal2.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix8_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_init_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q31.obj 

S_UPPER_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal2.d 

S_UPPER_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_bitreversal2.d" 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_bitreversal.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_bitreversal2.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_init_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_init_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_init_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_init_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_init_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_init_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix8_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_init_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_init_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_init_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_fast_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_fast_init_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_init_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_init_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_init_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_q31.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_bitreversal.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_init_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_init_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_init_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix2_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_init_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_init_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_init_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix4_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_cfft_radix8_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_init_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_init_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_init_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_dct4_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_fast_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_fast_init_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_init_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_init_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_init_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\TransformFunctions\arm_rfft_q31.d" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix8_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_init_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q31.c" 

S_UPPER_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal2.S" 


