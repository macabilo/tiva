################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_sin_cos_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_sin_cos_q31.c 

C_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_sin_cos_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_sin_cos_q31.d 

OBJS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_sin_cos_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_sin_cos_q31.obj 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_init_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_init_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_init_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_reset_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_reset_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_reset_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_sin_cos_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_sin_cos_q31.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_init_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_init_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_init_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_reset_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_reset_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_pid_reset_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_sin_cos_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ControllerFunctions\arm_sin_cos_q31.d" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_init_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_pid_reset_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_sin_cos_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ControllerFunctions/arm_sin_cos_q31.c" 


