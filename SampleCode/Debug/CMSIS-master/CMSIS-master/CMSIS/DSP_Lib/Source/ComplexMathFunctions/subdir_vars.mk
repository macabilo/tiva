################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q31.c 

C_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q31.d 

OBJS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q31.obj 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_conj_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_conj_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_conj_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_dot_prod_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_dot_prod_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_dot_prod_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_squared_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_squared_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_squared_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_cmplx_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_cmplx_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_cmplx_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_real_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_real_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_real_q31.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_conj_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_conj_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_conj_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_dot_prod_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_dot_prod_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_dot_prod_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_squared_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_squared_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mag_squared_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_cmplx_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_cmplx_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_cmplx_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_real_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_real_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\ComplexMathFunctions\arm_cmplx_mult_real_q31.d" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q31.c" 


