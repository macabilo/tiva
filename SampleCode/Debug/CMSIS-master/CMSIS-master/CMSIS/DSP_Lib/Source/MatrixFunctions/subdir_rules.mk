################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f64.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f64.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f64.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


