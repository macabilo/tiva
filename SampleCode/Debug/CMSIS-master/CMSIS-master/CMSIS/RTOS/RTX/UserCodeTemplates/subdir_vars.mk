################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MailQueue.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MemPool.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MsgQueue.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Mutex.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Semaphore.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Thread.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Timer.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/main.c 

C_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MailQueue.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MemPool.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MsgQueue.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Mutex.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Semaphore.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Thread.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Timer.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/main.d 

OBJS += \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MailQueue.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MemPool.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MsgQueue.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Mutex.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Semaphore.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Thread.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Timer.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/main.obj 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\MailQueue.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\MemPool.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\MsgQueue.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\Mutex.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\Semaphore.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\Thread.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\Timer.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\main.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\MailQueue.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\MemPool.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\MsgQueue.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\Mutex.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\Semaphore.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\Thread.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\Timer.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\UserCodeTemplates\main.d" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MailQueue.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MemPool.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/MsgQueue.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Mutex.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Semaphore.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Thread.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/Timer.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/UserCodeTemplates/main.c" 


