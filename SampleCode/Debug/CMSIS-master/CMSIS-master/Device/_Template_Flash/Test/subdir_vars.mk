################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/startup_stm32f10x_md.s 

C_SRCS += \
../CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashDev.c \
../CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashPrg.c \
../CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashTest.c \
../CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/system_stm32f10x.c 

S_DEPS += \
./CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/startup_stm32f10x_md.d 

C_DEPS += \
./CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashDev.d \
./CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashPrg.d \
./CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashTest.d \
./CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/system_stm32f10x.d 

OBJS += \
./CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashDev.obj \
./CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashPrg.obj \
./CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashTest.obj \
./CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/startup_stm32f10x_md.obj \
./CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/system_stm32f10x.obj 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\Device\_Template_Flash\Test\FlashDev.obj" \
"CMSIS-master\CMSIS-master\Device\_Template_Flash\Test\FlashPrg.obj" \
"CMSIS-master\CMSIS-master\Device\_Template_Flash\Test\FlashTest.obj" \
"CMSIS-master\CMSIS-master\Device\_Template_Flash\Test\startup_stm32f10x_md.obj" \
"CMSIS-master\CMSIS-master\Device\_Template_Flash\Test\system_stm32f10x.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\Device\_Template_Flash\Test\FlashDev.d" \
"CMSIS-master\CMSIS-master\Device\_Template_Flash\Test\FlashPrg.d" \
"CMSIS-master\CMSIS-master\Device\_Template_Flash\Test\FlashTest.d" \
"CMSIS-master\CMSIS-master\Device\_Template_Flash\Test\system_stm32f10x.d" 

S_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\Device\_Template_Flash\Test\startup_stm32f10x_md.d" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashDev.c" \
"../CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashPrg.c" \
"../CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/FlashTest.c" \
"../CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/system_stm32f10x.c" 

S_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/Device/_Template_Flash/Test/startup_stm32f10x_md.s" 


