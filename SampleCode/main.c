#include "tm4c123gh6pm.h"

void delay(int time);
/**
 * main.c
 */
int main(void)
{
    //1. Enable clock to Port F
    SYSCTL->RCGCGPIO = 0x00000020;

    //2. Set GPIO direction for Port F pins (LED pins)
    GPIOF->DIR = 0x0000000E;

    //3. Set LED pins as digital I/O
    GPIOF->DEN = 0x0000000E;

    while(1){
       GPIOF->DATA ^= 0xE;
       delay(1000);
    }
}


//FUNCTION DEFINITIONS
void delay(int time){
    int counter = 0;
    while(counter++ <= time*2000);
}
