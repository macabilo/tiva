# Tiva Project Templates
Date  Created: Nov 13, 2018  
Last Updated: Nov 13, 2018

Sample codes for different Tiva C (TM4C123GH6PM) functions. These codes were created using Code Composer 
Studio Version: 7.3.0.00019. This repository was intended to assist novice Tiva C programmers as well as
a reference for those who are familiar with the APIs.

